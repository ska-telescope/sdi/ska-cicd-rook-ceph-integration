#!/bin/sh
NAMESPACE=rook-ceph

for i in `kubectl -n ${NAMESPACE} get pods -o name`
do
  echo '-----------------------------------------'
  echo "Pod: $i"
  echo '-----------------------------------------'
  for j in `kubectl -n ${NAMESPACE} get $i -o jsonpath="{.spec.containers[*].name}"`
  do 
    echo "Container: $j"
    echo '#########################################'
    kubectl -n ${NAMESPACE} logs $i -c $j
    echo kubectl -n ${NAMESPACE} logs $i -c $j
    echo ''
  done
  echo ''
  echo ''
done
