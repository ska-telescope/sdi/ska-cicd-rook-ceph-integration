#!/bin/sh

NAMESPACE=default
CLASS=${CLASS:-nginx}

wait () {
    printf "*------------------------------ pause --------------------------------*\n\n"
    read X
}

echo "Check the Kubernetes cluster:"
printf "Connecting using KUBECONFIG=${KUBECONFIG}\n\nVersion Details:\n"
kubectl version
printf "\nList nodes:\n"
kubectl get nodes -o wide

printf "\nCheck the Ingress connection details:\n"
# get the external IP for the main loadbalancer - hopefully there is only one
# this then points to the Ingress controller
# If there isn't one then look for the NodePort implementation of NGINX
host=`kubectl cluster-info | grep 'control plane' | cut -d/ -f3 | cut -d: -f1`
port=`kubectl get svc ingress-nginx-controller -n ingress-nginx -o jsonpath="{.spec.ports[?(@.name=='http')].port}"`
TMP_EXTERNAL_IP="${host}:${port}"
EXTERNAL_IP=${EXTERNAL_IP:-${TMP_EXTERNAL_IP}}
echo "Ingress Controller LoadBalancer externalIP is: ${EXTERNAL_IP}"

printf "\n\nShow StorageClasses:\n"
kubectl get sc -o wide
printf "\nNext: show StorageClass details.\n"
# wait

printf "\nDeploy the Integration test:\n"
# create/test/destroy
cat <<EOF | sed "s/XCLASSX/${CLASS}/g" | kubectl -n ${NAMESPACE} apply -f -
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: pvc-test
spec:
  storageClassName: nfss1
  accessModes:
    - ReadWriteMany
  resources:
    requests:
      storage: 1Gi

---
apiVersion: v1
kind: ConfigMap
metadata:
  name: test
data:
  hello.conf: |
    server {
        listen 80;

        root /usr/share/nginx/html;
        try_files /index.html =404;

        expires -1;

        sub_filter_once off;
        sub_filter 'server_hostname' '$hostname';
        sub_filter 'server_address' '$server_addr:$server_port';
        sub_filter 'server_url' '$request_uri';
        sub_filter 'server_date' '$time_local';
        sub_filter 'request_id' '$request_id';
    }

---
apiVersion: v1
kind: Service
metadata:
  name: nginx1
  labels:
    app: nginx1
    app.kubernetes.io/name: test
spec:
  selector:
    app: nginx1
  ports:
    - protocol: TCP
      port: 80
      targetPort: 80

---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment1
  labels:
    app: nginx1
    app.kubernetes.io/name: test
spec:
  replicas: 3
  selector:
    matchLabels:
      app: nginx1
  template:
    metadata:
      labels:
        app: nginx1
        app.kubernetes.io/name: test
    spec:
      containers:
      - name: nginx
        image: nginx
        imagePullPolicy: Always
        ports:
          - containerPort: 80
            protocol: TCP
        volumeMounts:
          - mountPath: /usr/share/nginx/html
            name: www-data
            #readOnly: true
          - name: test-config
            mountPath: /etc/nginx/conf.d
      volumes:
      - name: www-data
        persistentVolumeClaim:
          claimName: pvc-test
      - name: test-config
        configMap:
          name: test

---
apiVersion: v1
kind: Service
metadata:
  name: nginx2
  labels:
    app: nginx2
    app.kubernetes.io/name: test
spec:
  selector:
    app: nginx2
  ports:
    - protocol: TCP
      port: 80
      targetPort: 80

---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment2
  labels:
    app: nginx2
    app.kubernetes.io/name: test
spec:
  replicas: 3
  selector:
    matchLabels:
      app: nginx2
  template:
    metadata:
      labels:
        app: nginx2
        app.kubernetes.io/name: test
    spec:
      containers:
      - name: nginx
        image: nginx
        imagePullPolicy: Always
        ports:
          - containerPort: 80
            protocol: TCP
        volumeMounts:
          - mountPath: /usr/share/nginx/html
            name: www-data
            readOnly: true
          - name: test-config
            mountPath: /etc/nginx/conf.d
      volumes:
      - name: www-data
        persistentVolumeClaim:
          claimName: pvc-test
      - name: test-config
        configMap:
          name: test

---
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: test
  labels:
    app.kubernetes.io/name: test
  annotations:
    kubernetes.io/ingress.class: "XCLASSX"
spec:
  rules:
  - http:
      paths:
      - path: /nginx1
        pathType: Prefix
        backend:
          service:
            name: nginx1
            port:
              number: 80
  - http:
      paths:
      - path: /nginx2
        pathType: Prefix
        backend:
          service:
            name: nginx2
            port:
              number: 80
EOF
kubectl -n ${NAMESPACE} get deployment,pod,svc,ingress.networking.k8s.io,pvc -l app.kubernetes.io/name=test -o wide
printf "\nNext: Check deployment.\n"
# wait

echo "Waiting for resources to deploy..."
kubectl -n ${NAMESPACE} wait --for=condition=available deployment.v1.apps/nginx-deployment1 --timeout=120s
kubectl -n ${NAMESPACE} wait --for=condition=available deployment.v1.apps/nginx-deployment2 --timeout=120s

kubectl -n ${NAMESPACE} get deployment,pod,svc,ingress.networking.k8s.io,pvc -l app.kubernetes.io/name=test -o wide
printf "\nNext: perform write/read test.\n"
# wait

echo "Perform write and then read test to/from shared storage: "
DTE=`date`
printf "\necho \"echo 'Wahoo: ${DTE}' > /usr/share/nginx/html/index.html\" | kubectl -n \${NAMESPACE} exec -i \$(kubectl get pods -l app=nginx1 -o name | head -1) -- bash\n\n"

echo "echo 'Wahoo: ${DTE}' > /usr/share/nginx/html/index.html" | kubectl -n ${NAMESPACE} exec -i $(kubectl get pods -l app=nginx1 -o name | head -1) -- bash

sleep 1

echo ''
echo '----------------------------------------nginx1----------------------------------------'
echo curl http://${EXTERNAL_IP}/nginx1
curl  http://${EXTERNAL_IP}/nginx1
echo ''
echo '----------------------------------------nginx2----------------------------------------'
echo curl http://${EXTERNAL_IP}/nginx2
curl http://${EXTERNAL_IP}/nginx2
echo ''
wait




printf "\nDeploy the RBD Integration test:\n"
# create/test/destroy
cat <<EOF | sed "s/XCLASSX/${CLASS}/g" | kubectl -n ${NAMESPACE} apply -f -
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: rbd-pvc
spec:
  accessModes:
  - ReadWriteOnce
  resources:
    requests:
      storage: 1Gi
  storageClassName: bds1

---
apiVersion: v1
kind: ConfigMap
metadata:
  name: rbd-test
data:
  hello.conf: |
    server {
        listen 80;

        root /usr/share/nginx/html;
        try_files /index.html =404;

        expires -1;

        sub_filter_once off;
        sub_filter 'server_hostname' '$hostname';
        sub_filter 'server_address' '$server_addr:$server_port';
        sub_filter 'server_url' '$request_uri';
        sub_filter 'server_date' '$time_local';
        sub_filter 'request_id' '$request_id';
    }

---
apiVersion: v1
kind: Service
metadata:
  name: rbd-nginx1
  labels:
    app: rbd-nginx1
spec:
  selector:
    app: rbd-nginx1
  ports:
    - protocol: TCP
      port: 80
      targetPort: 80

---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: rbd-nginx-deployment1
  labels:
    app: rbd-nginx1
spec:
  replicas: 1
  selector:
    matchLabels:
      app: rbd-nginx1
  template:
    metadata:
      labels:
        app: rbd-nginx1
    spec:
      containers:
      - name: nginx
        image: nginx
        imagePullPolicy: IfNotPresent
        ports:
          - containerPort: 80
            protocol: TCP
        volumeMounts:
          - mountPath: /usr/share/nginx/html
            name: www-data
          - name: rbd-test-config
            mountPath: /etc/nginx/conf.d
      volumes:
      - name: www-data
        persistentVolumeClaim:
          claimName: rbd-pvc
      - name: rbd-test-config
        configMap:
          name: rbd-test

---
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: rbd-test
  labels:
    app.kubernetes.io/name: rbd-test
  annotations:
    kubernetes.io/ingress.class: "XCLASSX"
spec:
  rules:
  - http:
      paths:
      - path: /nginx1
        pathType: Prefix
        backend:
          service:
            name: rbd-nginx1
            port:
              number: 80

EOF
kubectl -n ${NAMESPACE} get deployment,pod,svc,ingress.networking.k8s.io,pvc,pv -l app.kubernetes.io/name=test -o wide
printf "\nNext: Check deployment.\n"
# wait

echo "Waiting for resources to deploy..."
kubectl -n ${NAMESPACE} wait --for=condition=available deployment.v1.apps/rbd-nginx-deployment1 --timeout=120s

kubectl -n ${NAMESPACE} get deployment,pod,svc,ingress.networking.k8s.io,pvc -l app.kubernetes.io/name=test -o wide
printf "\nNext: perform write/read test.\n"
# wait

echo "Perform write and then read test to/from shared storage: "
DTE=`date`
printf "\necho \"echo 'Wahoo: ${DTE}' > /usr/share/nginx/html/index.html\" | kubectl -n \${NAMESPACE} exec -i \$(kubectl get pods -l app=nginx1 -o name | head -1) -- bash\n\n"

echo "echo 'Wahoo: ${DTE}' > /usr/share/nginx/html/index.html" | kubectl -n ${NAMESPACE} exec -i $(kubectl get pods -l app=rbd-nginx1 -o name | head -1) -- bash

sleep 1

echo ''
echo '----------------------------------------nginx1----------------------------------------'
echo curl http://${EXTERNAL_IP}/nginx1
curl  http://${EXTERNAL_IP}/nginx1
echo ''
wait

echo "Cleanup resources"
kubectl -n ${NAMESPACE} delete \
  ingress.networking.k8s.io/test \
  service/nginx1 \
  deployment.apps/nginx-deployment1 \
  service/nginx2 \
  deployment.apps/nginx-deployment2 \
  persistentvolumeclaim/pvc-test \
  configmap/test

# echo "Cleanup resources"
kubectl -n ${NAMESPACE} delete \
  ingress.networking.k8s.io/rbd-test \
  service/rbd-nginx1 \
  deployment.apps/rbd-nginx-deployment1 \
  persistentvolumeclaim/rbd-pvc \
  configmap/rbd-test

exit 0
