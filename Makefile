
ETC_CEPH ?= /etc/ceph
IMAGE ?= ceph/ceph:v15.2.6

# Args for K8s and charts
NAME := rook-ceph-storageclasses
KUBE_NAMESPACE ?= "rook-ceph"
KUBECONFIG ?=

# See : https://quay.io/repository/helmpack/chart-testing?tab=tags
CT_TAG ?= v3.0.0-beta.2
HELM_CHART = $(NAME)
HELM_RELEASE ?= v0.1.0
VALUES_FILE ?= charts/$(HELM_CHART)/values.yaml
SET_STANDARD ?= false # Create the "standard" StorageClass as copy of nfss1
INVENTORY_FILE ?= ./hosts
PRIVATE_VARS ?= ./rook_vars.yml
NODES ?= localhost
EXTRA_VARS ?=
COLLECTIONS_PATHS ?= ./collections

# Set dir of Makefile to a variable to use later
MAKEPATH := $(abspath $(lastword $(MAKEFILE_LIST)))
BASEDIR := $(patsubst %/,%,$(dir $(MAKEPATH)))

.PHONY: vars help test k8s show lint deploy delete logs describe namespace default all clean
.DEFAULT_GOAL := help

# define overides for above variables in here
-include PrivateRules.mak

ETC_CEPH_CLIENT_ADMIN_KEY_RING=$(ETC_CEPH)/ceph.client.admin.keyring

ANSIBLE_COLLECTIONS_PATHS := $(COLLECTIONS_PATHS):~/.ansible/collections:/usr/share/ansible/collections
K8S_COLLECTION := $(COLLECTIONS_PATHS)/ansible_collections/ska/systems_k8s

vars:  ## List Variables
	@echo "Current variable settings:"
	@echo "PRIVATE_VARS=$(PRIVATE_VARS)"
	@echo "INVENTORY_FILE=$(INVENTORY_FILE)"
	@echo "LOGGING_INVENTORY_FILE=$(LOGGING_INVENTORY_FILE)"
	@echo "EXTRA_VARS=$(EXTRA_VARS)"
	@echo "ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS)"
	@echo "K8S_COLLECTION=$(K8S_COLLECTION)"
	@echo "ETC_CEPH = $(ETC_CEPH)"
	@echo "ETC_CEPH_CLIENT_ADMIN_KEY_RING = $(ETC_CEPH_CLIENT_ADMIN_KEY_RING)"
	@echo "KUBECONFIG: $(KUBECONFIG)"
	@echo "HELM_CHART: $(HELM_CHART)"
	@echo "HELM_RELEASE: $(HELM_RELEASE)"
	@echo "VALUES_FILE: $(VALUES_FILE)"

k8s: vars ## Which kubernetes are we connected to
	@echo "Kubernetes cluster-info:"
	@kubectl cluster-info
	@echo ""
	@echo "kubectl version:"
	@kubectl version
	@echo ""
	@echo "Helm version:"
	@helm version --client
	@echo ""
	@echo "Helm plugins:"
	@helm plugin list

lint: lint_chart ## Lint check playbook
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-lint playbooks/rook.yml playbooks/roles/*  > ansible-lint-results.txt; \
	cat ansible-lint-results.txt

build_rook:  ## Install Rook
	ansible-playbook -i $(INVENTORY_FILE) playbooks/rook.yml --ask-become-pass \
	-e @$(PRIVATE_VARS) --extra-vars="rook_kubeconfig=$(KUBECONFIG) rook_namespace=$(KUBE_NAMESPACE)" \
	--extra-vars="$(EXTRA_VARS)" $(V)

rook: build_rook

delete_rook:
	kubectl delete CephCluster $(KUBE_NAMESPACE) -n $(KUBE_NAMESPACE) || true
	kubectl delete -f resources/common-external.yaml || true
	kubectl -n $(KUBE_NAMESPACE) delete -f resources/operator.yaml || true
	kubectl -n $(KUBE_NAMESPACE) delete -f resources/crds.yaml || true
	kubectl -n $(KUBE_NAMESPACE) delete -f resources/common.yaml || true
	kubectl delete ns $(KUBE_NAMESPACE) || true
	kubectl delete crd cephclients.ceph.rook.io || true
	kubectl delete crd cephclusters.ceph.rook.io || true



logs:  ## Check the rook-ceph logs
	./rooklogs.sh

tests:  ## rerun the CephFS and RBD tests
	@[ -n "$(KUBECONFIG)" ] || { echo "KUBECONFIG has not been set" && exit 1; }
	./integration-tests.sh

deploy: namespace  ## deploy the helm chart with kubectl
	@helm template $(HELM_RELEASE) charts/$(HELM_CHART)/ \
				--namespace $(KUBE_NAMESPACE) \
				 --values $(VALUES_FILE) \
				 --set classes.setStandard=$(SET_STANDARD) | \
				 kubectl -n $(KUBE_NAMESPACE) apply -f -

delete: ## delete the helm chart release
	@helm template $(HELM_RELEASE) charts/$(HELM_CHART)/ \
				--namespace $(KUBE_NAMESPACE) \
				 --values $(VALUES_FILE) \
				 --set classes.setStandard=$(SET_STANDARD) | \
				 kubectl -n $(KUBE_NAMESPACE) delete -f -

show: vars ## show the helm chart
	helm template $(HELM_RELEASE) charts/$(HELM_CHART)/ \
				 --namespace $(KUBE_NAMESPACE) \
				 --values $(VALUES_FILE) \
				 --set classes.setStandard=$(SET_STANDARD)

lint_chart: vars ## lint check the helm chart
	# Chart testing: https://github.com/helm/chart-testing
	helm lint charts/$(HELM_CHART)/ \
				 --namespace $(KUBE_NAMESPACE) \
				 --values $(VALUES_FILE)
	# @docker run --rm \
	#   --volume $(BASEDIR):/app \
	#   quay.io/helmpack/chart-testing:$(CT_TAG) \
	#   sh -c 'cd /app; ct lint --config ci/ct.yaml --all'

namespace: ## create the kubernetes namespace
	kubectl describe namespace $(KUBE_NAMESPACE) || kubectl create namespace $(KUBE_NAMESPACE)

delete_namespace: ## delete the kubernetes namespace
	@if [ "default" == "$(KUBE_NAMESPACE)" ] || [ "kube-system" == "$(KUBE_NAMESPACE)" ]; then \
	echo "You cannot delete Namespace: $(KUBE_NAMESPACE)"; \
	exit 1; \
	else \
	kubectl describe namespace $(KUBE_NAMESPACE) && kubectl delete namespace $(KUBE_NAMESPACE); \
	fi

help:  ## show this help.
	@echo "make targets:"
	@grep -E '^[0-9a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ": .*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
	@echo ""; echo "make vars (+defaults):"
	@grep -E '^[0-9a-zA-Z_-]+ \?=.*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = " \\?\\= "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
