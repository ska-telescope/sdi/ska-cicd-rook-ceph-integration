# rook-ceph-integration

Deploy Rook.Io for Ceph onto a Kubernetes Cluster.

## Summary

This repo deploys Rook.io for External Ceph integration onto a Kubernetes cluster by using https://rook.io/docs/rook/v1.3/ceph-cluster-crd.html (search for External Cluster).

Checkout rook-ceph-integration (this repo) and the `rook` submodule (https://github.com/rook/rook) with:
```
$ git clone git@gitlab.com:ska-telescope/sdi/rook-ceph-integration.git
$ cd rook-ceph-integration
$ git submodule update --init --recursive
```

Setup your environment to point to the appropriate Kubernetes cluster (setting  KUBECONFIG is **mandatory**):
```
$ export KUBECONFIG=/path/to/kubernetes/config
```

Install the Ansible collections:
```
$ make install
```
(there is also the `make reinstall` target that could be used to update the current versions of the collections)

Additionally, the Ceph config files `/etc/ceph/ceph.conf`, and `/etc/ceph/ceph.client.admin.keyring` must exist and be readable by the current shell.

It is imperative that the external Ceph cluster version (check with `ceph version`) is an exact match for the version defined for the spec.cephVersion.image in `resources/cluster-external-only.yaml`  of the `CephCluster` resource.
eg: `ceph/ceph:v14.2.8` matches `ceph version 14.2.8 (2d095e947a02261ce61424021bb43bd3022d35cb) nautilus (stable)`.

The deployment is triggered with (adding the IMAGE value here as example):
```
$ make build IMAGE=ceph/ceph:v14.2.8
```

## make help

Run `make` to get the help:
```
$ make
make targets:
build                          Run the external ceph integration deloyment script AND deploy chart and run tests
delete                         delete the helm chart release
delete_namespace               delete the kubernetes namespace
deploy                         deploy the helm chart with kubectl
help                           show this help.
install                        alias for deploy
k8s                            Which kubernetes are we connected to
lint                           lint check the helm chart
logs                           Check the rook-ceph logs
namespace                      create the kubernetes namespace
show                           show the helm chart
tests                          rerun the CephFS and RBD tests
vars                           List Variables

make vars (+defaults):
CT_TAG                         v3.0.0-beta.2
HELM_RELEASE                   v0.1.0
KUBE_NAMESPACE                 "rook-ceph"
SET_STANDARD                   false # Create the "standard" StorageClass as copy of nfss1
VALUES_FILE                    charts/$(HELM_CHART)/values.yaml
```

## Checking the deployment

The Rook.Io for Ceph deployment can be checked throught the logs using:
```
$ make logs
```
This will iterate through all the components deployed in the `rook-ceph` namespace and print the logs of these Pods.


## The tests

Two test are run as part of the deployment:
* mount-test-cephfs - multi-mount network share on CephFS
* mount-testrbd - Rados Block Device single mount filesystem

To run these, go:
```
$ make tests 
./integration-tests.sh
Ingress Controller LoadBalancer externalIP is: 192.168.100.65
persistentvolumeclaim/cephfs-pvc created
configmap/cephfs-test created
service/cephfs-nginx1 created
deployment.apps/cephfs-nginx-deployment1 created
service/cephfs-nginx2 created
deployment.apps/cephfs-nginx-deployment2 created
ingress.extensions/cephfs-test created
deployment.apps/cephfs-nginx-deployment1 condition met
deployment.apps/cephfs-nginx-deployment2 condition met

---------cephfs-nginx1---------
Wahoo[cephfs]: Thu Apr 16 03:14:23 UTC 2020
---------cephfs-nginx2---------
Wahoo[cephfs]: Thu Apr 16 03:14:23 UTC 2020
-------------------------------

persistentvolumeclaim "cephfs-pvc" deleted
configmap "cephfs-test" deleted
service "cephfs-nginx1" deleted
deployment.apps "cephfs-nginx-deployment1" deleted
service "cephfs-nginx2" deleted
deployment.apps "cephfs-nginx-deployment2" deleted
ingress.extensions "cephfs-test" deleted
persistentvolumeclaim/rbd-pvc created
configmap/rbd-test created
service/rbd-nginx1 created
deployment.apps/rbd-nginx-deployment1 created
ingress.extensions/rbd-test created
deployment.apps/rbd-nginx-deployment1 condition met

-------------------------------
Wahoo[rbd]: Thu Apr 16 03:15:13 UTC 2020
-------------------------------

persistentvolumeclaim "rbd-pvc" deleted
configmap "rbd-test" deleted
service "rbd-nginx1" deleted
deployment.apps "rbd-nginx-deployment1" deleted
ingress.extensions "rbd-test" deleted
```

Check the output of the curl commands, where `Wahoo` has been written on the the filesystem via the Pod, and then pulled back out with an HTTP call.

## Rook Ceph documentation

There are three features that have been installed:

* Block Device - https://rook.io/docs/rook/v1.3/ceph-block.html
* CephFS - https://rook.io/docs/rook/v1.3/ceph-filesystem.html
* Object Storage - https://rook.io/docs/rook/v1.3/ceph-object.html

See the tests in `resources/` for working examples using the related StorageClasses.

## Installing the StorageClasses

The StorageClasses that expose `RDB` and `CephFS` storage to Kubernetes are installed as part of `make build`, but can be installed independently using:
```
$make install
```

The StorageClasses are as follows:

| Classname     | Maps to        | Usage                                      |
| ------------- |----------------| -------------------------------------------|
| nfss1         | CephFS         | Shared Network Filesystem - ReadWriteMany  |
| nfs           | alias to nfss1 | Shared Network Filesystem - ReadWriteMany  |
| bds1          | RBD            | Single concurrent use ext4 - ReadWriteOnce |
| block         | alias to bds1  | Single concurrent use ext4 - ReadWriteOnce |


StorageClass naming convention follows the following pattern:
```
<xxx type - bd=block device, nfs=network filesystem><x class - s=standard,i=iops optimised (could be ssd/nvme), t=throughput optimised (could be hdd, or cheaper ssd)><n version - 1=first version,...>[-<location - future tag for denoting location context, rack, dc, etc>]
```

* bds1
  - block device - single mount (ReadWriteOnce)
  - standard
  - version 1
* nfss1
  - network filesystem enabled storage (ReadWriteMany)
  - standard
  - version 1
* block = shortcut for bds1
* nfs = shortcut for nfss1

